# myenv-setup

Setup DaikiMaekawa's environment

[![Build Status](https://travis-ci.org/DaikiMaekawa/myenv-setup.svg?branch=master)](https://travis-ci.org/DaikiMaekawa/myenv-setup)
## Usage

```sh
$ ./setup.sh
```

## Documents

https://github.com/DaikiMaekawa/myenv-setup/tree/master/doc

## License

Copyright (c) 2014, [Daiki Maekawa](http://daikimaekawa.strikingly.com/). (MIT License)

See LICENSE for more info.
